# Задания по курсу Технологии и практики MLOps
Данный репозиторий предназначен для инфраструктуры

## Команда
Фёдорова Инесса

## MinIO

![image](imgs/minio.png)

## MLflow

![image](imgs/mlflow_experiments.png)


## Мониторинг

Подняты и настроены prometheus и grafana для мониторинга сервиса

![image](imgs/monitoring.png)
